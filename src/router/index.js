import Vue from 'vue'
import VueRouter from 'vue-router'
import Building from "../pages/Building";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'building',
    component: Building
  },
  {
    path: '/residences',
    name: 'residences',
    component: () => import('../pages/Residences.vue')
  },
  {
    path: '/penthouse',
    name: 'penthouse',
    component: () => import('../pages/Penthouse.vue')
  },
  {
    path: '/neighborhood',
    name: 'neighborhood',
    component: () => import('../pages/Neighborhood.vue')
  },
  {
    path: '/map',
    name: 'map',
    component: () => import('../pages/MapPage.vue')
  },
  {
    path: '/availability',
    name: 'availability',
    component: () => import('../pages/Availability.vue')
  },
  {
    path: '/team',
    name: 'team',
    component: () => import('../pages/Team.vue')
  },
  {
    path: '/contact',
    name: 'contact',
    component: () => import('../pages/Contact.vue')
  }
];

const router = new VueRouter({
  routes
});

export default router
